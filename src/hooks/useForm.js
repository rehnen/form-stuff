import { useReducer, useCallback } from 'react';

const defaultOnSubmit = values => values;

export const modes = {
  ON_FIRST_INPUT: 'ON_FIRST_INPUT',
  AFTER_SUBMIT: 'AFTER_SUBMIT',
  ON_BLUR: 'ON_BLUR',
  ON_BLUR_AFTER_CHANGE: 'ON_BLUR_AFTER_CHANGE',
  IMMEDIATE: 'IMMEDIATE',
};

const validate = (mode, value, { touched, changed, submitted }, validation) => {
  if (mode === modes.IMMEDIATE) {
    return typeof validation === 'function' ? validation(value) : value.match(validation);
  }
  if (mode === modes.ON_BLUR && touched) {
    return typeof validation === 'function' ? validation(value) : value.match(validation);
  }
  if (mode === modes.ON_FIRST_INPUT && changed) {
    return typeof validation === 'function' ? validation(value) : value.match(validation);
  }
  if (mode === modes.ON_BLUR_AFTER_CHANGE && changed && touched) {
    return typeof validation === 'function' ? validation(value) : value.match(validation);
  }
  if (mode === modes.AFTER_SUBMIT && submitted) {
    return typeof validation === 'function' ? validation(value) : value.match(validation);
  }

  return true;
};

const inputReducer = (state, { type, payload }) => {
  switch (type) {
  case 'change': {
    const {
      touched,
      submitted,
    } = state.values[payload.name];
    return {
      ...state,
      values: {
        ...state.values,
        [payload.name]: {
          ...state.values[payload.name],
          value: payload.value,
          changed: true,
          valid: validate(state.mode,
            payload.value,
            {
              touched,
              changed: true,
              submitted,
            },
            payload.validation),
        },
      },
    };
  }
  case 'touched': {
    const {
      changed,
      submitted,
    } = state.values[payload.name];
    return {
      ...state,
      values: {
        ...state.values,
        [payload.name]: {
          ...state.values[payload.name],
          touched: state.mode !== modes.ON_BLUR_AFTER_CHANGE || changed,
          valid: validate(state.mode,
            state.values[payload.name].value,
            {
              touched: state.mode !== modes.ON_BLUR_AFTER_CHANGE || changed,
              changed,
              submitted,
            },
            payload.validation),
        },
      },
    };
  }
  case 'set_error': {
    const { values } = state;
    Object.entries(payload).forEach(([k, v]) => {
      values[k].valid = v;
      values[k].changed = true;
      values[k].touched = true;
    });
    return {
      ...state,
      values: {
        ...values,
      },
    };
  }
  default:
    return state;
  }
};

const useForm = ({
  inputs,
  onSubmit = defaultOnSubmit,
  mode = modes.ON_BLUR_AFTER_CHANGE,
}) => {
  const values = Object.entries(inputs)
    .reduce((acc, [name, obj]) => {
      acc[name] = {
        value: obj.value || '',
        valid: obj.valid || true,
        changed: false,
        touched: mode === modes.IMMEDIATE,
      };
      return acc;
    }, {});

  const [state, dispatch] = useReducer(inputReducer, { values, mode });

  const onChange = useCallback((event) => {
    const { name, value } = event.target;
    dispatch({ type: 'change', payload: { name, value, validation: inputs[name].validate } });
  }, []);

  const onBlur = useCallback(({ target: { name } }) => {
    dispatch({ type: 'touched', payload: { name, validation: inputs[name].validate } });
  }, []);

  const setErrors = useCallback((payload) => {
    dispatch({ type: 'set_error', payload });
  }, []);

  const useInput = name => ({
    onBlur,
    onChange,
    value: state.values[name].value,
    error: !state.values[name].valid,
    name,
  });

  const valuesFromState = Object.entries(state.values).reduce((acc, [key, val]) => {
    acc[key] = val.value;
    return acc;
  }, {});
  const submit = () => onSubmit(valuesFromState);

  return {
    useInput,
    submit,
    setErrors,
  };
};

export default useForm;
