import React from 'react';
import axios from 'axios';
import Input from './formComponents/Input';
import useForm from '../hooks/useForm';

const LoginForm = () => {
  const { useInput, submit, setErrors } = useForm({
    inputs: {
      userName: {
        validate: value => value !== '',
      },
      email: {
        validate: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
      },
      password: {
        validate: value => value !== 'Pizza',
      },
    },
    onSubmit: ({ userName, password }) => axios
      .post('api/login', { userName, password })
      .then(console.log)
      .catch(console.error),
  });

  const errorCallback = () => setErrors({ userName: false, password: false });

  return (
    <div>
      <h2>Login form</h2>
      <form>
        <label htmlFor="userName">
          Username
          <Input id="userName" {...useInput('userName')} helpText="Please enter your username" />
        </label>
        <label htmlFor="email">
          Email
          <Input id="email" {...useInput('email')} helpText="Please enter your email" />
        </label>
        <label htmlFor="password">
          Password
          <Input id="password" {...useInput('password')} helpText="Please enter your password" />
        </label>
        <button type="button" onClick={submit}>Hej</button>
        <button type="button" onClick={errorCallback}>Set errors</button>
      </form>
    </div>
  );
};

export default LoginForm;
