import React from 'react';
import PropTypes from 'prop-types';


const Input = ({
  value,
  name,
  error,
  onChange,
  onBlur,
  id,
  helpText,
}) => (
  <div>
    <input
      id={id}
      name={name}
      onBlur={onBlur}
      onChange={onChange}
      value={value}
      style={{
        borderColor: error ? 'red' : null,
        borderWidth: 5,
        borderStyle: 'solid',
        width: 'fit-content',
      }}
    />
    {error && helpText && (
      <span>{helpText}</span>
    )}
  </div>);

Input.defaultProps = {
  error: false,
  helpText: null,
};

Input.propTypes = {
  value: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  error: PropTypes.bool,
  onBlur: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  helpText: PropTypes.string,
};

export default React.memo(Input);
