module.exports = {
    "extends": "airbnb",
    "env": {
        "jest": true,
    },
    "rules": {
        indent: ['error', 2],
        "react/jsx-indent": ['error', 2],
        "jsx-a11y/label-has-associated-control": [ "error", {
            "assert": "either",
            "depth": 25,
          }],
          "jsx-a11y/label-has-for": 0,
    },
    globals: {
        'document': false,
        Headers: false,
        fetch: false,
    }
};